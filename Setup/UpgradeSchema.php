<?php 

/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MagePayment\Stripe\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Upgrade the Catalog module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.1', '<=')) {
            $table = $setup->getConnection()
              ->newTable($setup->getTable('magepayment_stripe_customer'))
              ->addColumn(
                  'customer_id',
                  \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                  null,
                  ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                  'Customer ID'
              )
              ->addColumn(
                  'stripe_customer_id',
                  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                  null,
                  ['nullable' => false, 'default' => ''],
                  'Stripe Customer Id'
              )
              ->addColumn(
                  'stripe_customer_token',
                  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                  null,
                  ['nullable' => false, 'default' => ''],
                  'Stripe Customer Token'
              )
              ->addColumn(
                  'stripe_customer_email',
                  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                  128,
                  ['nullable' => false, 'default' => ''],
                  'Stripe Customer Email'
              )
              ->addColumn(
                  'created_at',
                  \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                  null,
                  [
                    'nullable' => false, 
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                  ],
                  'Create Time'
              )
              ->addColumn(
                  'updated_at',
                  \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                  null,
                  [
                    'nullable' => false, 
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE
                  ],
                  'Update Time'
              )->setComment("magepayment stripe customer");
              $setup->getConnection()->createTable($table);
      }
      $setup->endSetup();
    }
}