<?php

namespace MagePayment\Stripe\Model;

class Customer extends \Magento\Framework\Model\AbstractModel 
{
	
	protected function _construct()
	{
		$this->_init('MagePayment\Stripe\Model\ResourceModel\Customer');
	}

	
}