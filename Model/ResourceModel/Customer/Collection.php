<?php
namespace MagePayment\Stripe\Model\ResourceModel\Customer;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('MagePayment\Stripe\Model\Customer', 'MagePayment\Stripe\Model\ResourceModel\Customer');
	}

}