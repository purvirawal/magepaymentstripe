<?php
namespace MagePayment\Stripe\Model\ResourceModel;


class Customer extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	
	protected function _construct()
	{
		$this->_init('magepayment_stripe_customer', 'customer_id');
	}
	
}



